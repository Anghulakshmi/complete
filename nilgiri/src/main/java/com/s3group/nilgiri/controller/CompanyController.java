package com.s3group.nilgiri.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.s3group.nilgiri.dto.CompanyDTO;
import com.s3group.nilgiri.dto.ResponseDTO;

import com.s3group.nilgiri.service.CompanyService;

@RestController
@RequestMapping("/api")

public class CompanyController {
	
	
public static final Logger logger = LoggerFactory.getLogger(CompanyController.class);
	
	@Autowired
	private CompanyService companyService; 
	
	@RequestMapping(value = "/company/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompanyDTO>> listAllCompanys() {
		logger.info("Getting All Companys.");			
		List<CompanyDTO> companyList = companyService.findAllCompanys();
		if (companyList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
		return new ResponseEntity<List<CompanyDTO>>(companyList, HttpStatus.OK);
	}
	
	
	 @RequestMapping(value = "/company/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity<ResponseDTO> createCompany(@RequestBody @Valid CompanyDTO company) {       
         logger.info("Creating Company : {}", company);    
         
         logger.info("Creating Company : {}", company.getcompany());
         
         List<CompanyDTO> companys = companyService.saveCompany(company);
         
         //logger.info("save successful in createCompany method", company.getcompany());
         
         ResponseDTO res = new ResponseDTO();
         if (companys.isEmpty())
         {
         res.setError_code("1");
         res.setStatus("EMPTY");
         }
         else
         {
             res.setError_code("0");
             res.setStatus("CREATED");
             res.setData(companys);
         }
        
         return new ResponseEntity<ResponseDTO>(res, HttpStatus.CREATED);
     }


	@RequestMapping(value = "/company/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> updateCompany(@RequestBody CompanyDTO company) {
		logger.info("Updating Company with id {}", company.getId());
		List<CompanyDTO> companys = companyService.updateCompany(company);
		  ResponseDTO res = new ResponseDTO();
	         if (companys.isEmpty())
	         {
	         res.setError_code("1");
	         res.setStatus("EMPTY");
	         }
	         else
	         {
	             res.setError_code("0");
	             res.setStatus("CREATED");
	             res.setData(companys);
	         }
		return new ResponseEntity<ResponseDTO>(res, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/company/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteCompany(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Company with id {}", id);
		companyService.deleteCompany(id);
		return new ResponseEntity<String>("DELETED", HttpStatus.OK);
	}
	

}
