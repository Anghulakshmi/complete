package com.s3group.nilgiri.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.s3group.nilgiri.dto.DivisionDTO;
import com.s3group.nilgiri.dto.ResponseDTO;
import com.s3group.nilgiri.service.DivisionService;

@RestController
@RequestMapping("/api")

public class DivisionController {
	
	
public static final Logger logger = LoggerFactory.getLogger(DivisionController.class);
	
	@Autowired
	private DivisionService divisionService; 
	
	@RequestMapping(value = "/division/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DivisionDTO>> listAllDivisions() {
		logger.info("Getting All Divisions.");			
		List<DivisionDTO> divisionList = divisionService.findAllDivisions();
		if (divisionList.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
		return new ResponseEntity<List<DivisionDTO>>(divisionList, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/division/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> createDivision(@RequestBody @Valid DivisionDTO division) {		
		logger.info("Creating Division : {}", division);		
		List<DivisionDTO> divisions = divisionService.saveDivision(division);
		 ResponseDTO res = new ResponseDTO();
         if (divisions.isEmpty())
         {
         res.setError_code("1");
         res.setStatus("EMPTY");
         }
         else
         {
             res.setError_code("0");
             res.setStatus("CREATED");
             res.setData(divisions);
         }
		return new ResponseEntity<ResponseDTO>(res, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/division/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> updateDivision(@RequestBody DivisionDTO division) {
		logger.info("Updating division with id {}", division.getId());
		List<DivisionDTO> divisions = divisionService.updateDivision(division);
		 ResponseDTO res = new ResponseDTO();
         if (divisions.isEmpty())
         {
         res.setError_code("1");
         res.setStatus("EMPTY");
         }
         else
         {
             res.setError_code("0");
             res.setStatus("CREATED");
             res.setData(divisions);
         }
		return new ResponseEntity<ResponseDTO>(res, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/division/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> deleteDivision(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Division with id {}", id);
		divisionService.deleteDivision(id);
		return new ResponseEntity<String>("DELETED", HttpStatus.OK);
	}
	

}
