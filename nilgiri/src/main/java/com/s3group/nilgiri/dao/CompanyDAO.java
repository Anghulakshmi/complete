package com.s3group.nilgiri.dao;

import java.util.List;

import com.s3group.nilgiri.entity.Company;

public interface CompanyDAO {

	public List<Company> findAllCompanys();
	
	public List<Company> saveCompany(Company company);
	
	public List<Company> updateCompany(Company company);
	
	public void deleteCompany(Long id);
	
	public Company findByCompanyId(Long id);
}
