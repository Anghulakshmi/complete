package com.s3group.nilgiri.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.s3group.nilgiri.entity.Company;


@Repository
@Transactional

public class CompanyDAOImpl implements CompanyDAO {
	
public static final Logger logger = LoggerFactory.getLogger(CompanyDAOImpl.class);
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public Company findByCompanyId(Long id) {
		return entityManager.find(Company.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> findAllCompanys(){		
		String hql = "FROM Company as ct ORDER BY ct.companyId";
		return (List<Company>) entityManager.createQuery(hql).getResultList();		
	}
	
	@Override
	public List<Company> saveCompany(Company company) {	
		List<Company> Companys = new ArrayList<Company>();
		try {
			logger.info("Company = "+company.getCompcompany());
			/*logger.info("Company = "+company.getCompaddress1());
			logger.info("Company = "+company.getCompanyId());
			logger.info("Company = "+company.getCompzip());
			logger.info("Company = "+company.getCompcity()); */
			entityManager.persist(company);	
			logger.info("Persisted");
			Companys = findAllCompanys();
			logger.info("CompanysSize = "+Companys.size());
		}catch(Exception e) {e.printStackTrace();}
		
		return Companys;
	}
	
	@Override
	public List<Company> updateCompany(Company company) {
		List<Company> companys = new ArrayList<Company>();
		Company ct = findByCompanyId(company.getCompanyId());		
		if(ct == null) {
			logger.error("Unable to update. Company with id {} not found.", company.getCompanyId());
		}else {			
			logger.info("Existing Desc = " +ct.getCompdesc()+" New Desc = "+company.getCompdesc());
			entityManager.merge(company);
			logger.info("Updated");
			companys = findAllCompanys();
		}
		
		return companys;
	}
	
	@Override
	public void deleteCompany(Long id) {		
		entityManager.remove(findByCompanyId(id));
		logger.info("Deleted");	
	
	}
	

}
