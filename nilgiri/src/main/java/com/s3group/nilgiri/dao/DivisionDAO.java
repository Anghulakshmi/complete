package com.s3group.nilgiri.dao;

import java.util.List;

import com.s3group.nilgiri.entity.Division;

public interface DivisionDAO {

	public List<Division> findAllDivisions();
	
	public List<Division> saveDivision(Division division);
	
	public List<Division> updateDivision(Division division);
	
	public void deleteDivision(Long id);
	
	public Division findByDivisionId(Long id);
}
