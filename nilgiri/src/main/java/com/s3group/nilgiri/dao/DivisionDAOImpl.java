package com.s3group.nilgiri.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.s3group.nilgiri.entity.Division;

@Repository
@Transactional
public class DivisionDAOImpl implements DivisionDAO {
	
public static final Logger logger = LoggerFactory.getLogger(DivisionDAOImpl.class);
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public Division findByDivisionId(Long id) {
		return entityManager.find(Division.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Division> findAllDivisions(){		
		String hql = "FROM Division as ct ORDER BY ct.divisionId";
		return (List<Division>) entityManager.createQuery(hql).getResultList();		
	}
	
	@Override
	public List<Division> saveDivision(Division division) {	
		List<Division> divisions = new ArrayList<Division>();
		try {
			logger.info("Division = "+division.getDivndivision());
			entityManager.persist(division);	
			logger.info("Persisted");
			divisions = findAllDivisions();
			logger.info("DivisionsSize = "+divisions.size());
		}catch(Exception e) {e.printStackTrace();}
		
		return divisions;
	}
	
	@Override
	public List<Division> updateDivision(Division division) {
		List<Division> divisions = new ArrayList<Division>();
		Division ct = findByDivisionId(division.getDivisionId());		
		if(ct == null) {
			logger.error("Unable to update. Division with id {} not found.", division.getDivisionId());
		}else {			
			logger.info("Existing Desc = " +ct.getDivndesc()+" New Desc = "+division.getDivndesc());
			entityManager.merge(division);
			logger.info("Updated");
			divisions = findAllDivisions();
		}
		
		return divisions;
	}
	
	@Override
	public void deleteDivision(Long id) {		
		entityManager.remove(findByDivisionId(id));
		logger.info("Deleted");	
	
	}

}
