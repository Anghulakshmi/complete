package com.s3group.nilgiri.dao;

import java.util.List;

import com.s3group.nilgiri.entity.Location;


public interface LocationDAO {
	
	public List<Location> findAllLocations();
	
	public List<Location> saveLocation(Location counter);
	
	public List<Location> updateLocation(Location counter);
	
	public void deleteLocation(Long id);
	
	public Location findByLocationId(Long id);

}
