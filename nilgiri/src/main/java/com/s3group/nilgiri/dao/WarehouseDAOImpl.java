package com.s3group.nilgiri.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.s3group.nilgiri.entity.Warehouse;

@Repository
@Transactional
public class WarehouseDAOImpl implements WarehouseDAO {
	
	public static final Logger logger = LoggerFactory.getLogger(WarehouseDAOImpl.class);
	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public Warehouse findByWarehouseId(Long warehouseId) {
		return entityManager.find(Warehouse.class, warehouseId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Warehouse> findAllWarehouses(){		
		String hql = "FROM Warehouse as wh ORDER BY wh.warehouseId";
		return (List<Warehouse>) entityManager.createQuery(hql).getResultList();		
	}
	
	@Override
	public List<Warehouse> saveWarehouse(Warehouse warehouse) {	
		List<Warehouse> warehouses = new ArrayList<Warehouse>();
		try {
			logger.info("Warehouse = "+warehouse.getWhseWarehouse());
			entityManager.persist(warehouse);	
			logger.info("Persisted");
			warehouses = findAllWarehouses();
			logger.info("WarehouseSize = "+warehouses.size());
		}catch(Exception e) {e.printStackTrace();}
		
		return warehouses;
	}
	
	@Override
	public List<Warehouse> updateWarehouse(Warehouse warehouse) {
		List<Warehouse> warehouses = new ArrayList<Warehouse>();
		Warehouse wh = findByWarehouseId(warehouse.getWarehouseId());		
		if(wh == null) {
			logger.error("Unable to update. Warehouse with id {} not found.", warehouse.getWhseWarehouse());
		}else {			
			logger.info("Existing Desc = " +wh.getWhseDesc()+" New Desc = "+warehouse.getWhseDesc());
			entityManager.merge(warehouse);
			logger.info("Updated");
			warehouses = findAllWarehouses();
		}
		
		return warehouses;
	}
	
	@Override
	public void deleteWarehouse(Long id) {		
		entityManager.remove(findByWarehouseId(id));
		logger.info("Deleted");	
	
	}
}