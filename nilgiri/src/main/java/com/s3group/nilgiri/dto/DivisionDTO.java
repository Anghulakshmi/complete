package com.s3group.nilgiri.dto;


public class DivisionDTO {
	
	private Long companyId;
	private Long Id;
	private String division;
	private String desc;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String contactname;
	private String contactnumber;
	
	public DivisionDTO() {}
	
	public Long getId() {
		return Id;
	}

	public void setId(Long Id) {
		this.Id = Id;
	}
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	/*public Company getcompany() {
		return company;
	}
	
	public void setcompany(Company company)
	{
		this.company=company;
	}*/

	public String getdivision() {
		return division;
	}

	public void setdivision(String division) {
		this.division = division;
	}
	
	public String getdesc() {
		return desc;
	}

	public void setdesc(String desc) {
		this.desc = desc;
	}

	public String getaddress1() {
		return address1;
	}

	public void setaddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getaddress2() {
		return address2;
	}

	public void setaddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getcity() {
		return city;
	}

	public void setcity(String city) {
		this.city = city;
	}
	
	public String getstate() {
		return state;
	}

	public void setstate(String state) {
		this.state = state;
	}
	
	public String getzip() {
		return zip;
	}

	public void setzip(String zip) {
		this.zip = zip;
	}
	
	public String getcountry() {
		return country;
	}

	public void setcountry(String country) {
		this.country = country;
	}
	
	public String getcontactname() {
		return contactname;
	}

	public void setcontactname(String contactname) {
		this.contactname = contactname;
	}
	
	public String getcontactnumber() {
		return contactnumber;
	}

	public void setcontactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	
}
