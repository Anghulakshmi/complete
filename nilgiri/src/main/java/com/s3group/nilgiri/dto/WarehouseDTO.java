package com.s3group.nilgiri.dto;

public class WarehouseDTO {
	
	private Long companyId;
	private Long divisionId;
	private Long warehouseId;
	private String warehouse;
    private String desc;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String contactname;
	private String contactnumber;
	private String altaddress1;
	private String altaddress2;
	private String altcity;
	private String altstate;
	private String altzip;
	private String altcountry;


public WarehouseDTO() {}

public Long getCompanyId() {
	return companyId;
}
public void setCompanyId(Long companyId) {
	this.companyId = companyId;
}
public Long getDivisionId() {
	return divisionId;
}
public void setDivisionId(Long divisionId) {
	this.divisionId = divisionId;
}

public Long getWarehouseId() {
	return warehouseId;
}
public void setWarehouseId(Long warehouseId) {
	this.warehouseId = warehouseId;
}

public String getWarehouse() {
	return warehouse;
}
public void setWarehouse(String warehouse) {
	this.warehouse = warehouse;
}
public String getDesc() {
	return desc;
}
public void setDesc(String description) {
	this.desc = description;
}
public String getAddress1() {
	return address1;
}
public void setAddress1(String address1) {
	this.address1 = address1;
}
public String getAddress2() {
	return address2;
}
public void setAddress2(String address2) {
	this.address2 = address2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getZip() {
	return zip;
}
public void setZip(String zip) {
	this.zip = zip;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getContactName() {
	return contactname;
}
public void setContactName(String contactname) {
	this.contactname = contactname;
}
public String getContactNumber() {
	return contactnumber;
}
public void setContactNumber(String contactnumber) {
	this.contactnumber = contactnumber;
}
public String getAltAddress1() {
	return altaddress1;
}
public void setAltAddress1(String altaddress1) {
	this.altaddress1 = altaddress1;
}

public String getAltAddress2() {
	return altaddress2;
}
public void setAltAddress2(String altaddress2) {
	this.altaddress2 = altaddress2;
}
public String getAltCity() {
	return altcity;
}
public void setAltCity(String altcity) {
	this.altcity = altcity;
}

public String getAltState() {
	return altstate;
}
public void setAltState(String altstate) {
	this.altstate = altstate;
}
public String getAltZip() {
	return altzip;
}
public void setAltZip(String altzip) {
	this.altzip = altzip;
}
public String getAltCountry() {
	return altcountry;
}
public void setAltCountry(String altcountry) {
	this.altcountry = altcountry;
}


}