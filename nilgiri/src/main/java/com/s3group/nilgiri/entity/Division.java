package com.s3group.nilgiri.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;


/*import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;*/

@Entity
@Table(name="DIVISION")
public class Division implements Serializable{
	
	 /*@ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "COMPANY_ID", nullable=false)	
	 
	 private Company company;
	 
	 public Division() {
		 
	 }*/
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name="DIVISION_ID")
	private Long divisionId;
	
	@Column(name="COMPANY_ID")
	private Long companyId;
	
	@Column(name="DIVN_DIVISION")
	private String divndivision;
	
	@Column(name="DIVN_DESC")
	private String divndesc;
	
	@Column(name="DIVN_ADDRESS1")
	private String divnaddress1;
	
	@Column(name="DIVN_ADDRESS2")
	private String divnaddress2;
	
	@Column(name="DIVN_CITY")
	private String divncity;
	
	@Column(name="DIVN_STATE")
	private String divnstate;
	
	@Column(name="DIVN_ZIP")
	private String divnzip;
	
	@Column(name="DIVN_COUNTRY")
	private String divncountry;
	
	@Column(name="DIVN_CONTACT_NAME")
	private String divncontactname;
	
	@Column(name="DIVN_CONTACT_NUMBER")
	private String divncontactnumber;
	
	public Long getDivisionId() {
		return divisionId;
	}

	public void setDivisionId(Long divisionId) {
		this.divisionId = divisionId;
	}
	
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	
	public String getDivndivision() {
		return divndivision;
	}

	public void setDivndivision(String divndivision) {
		this.divndivision = divndivision;
	}
	
	public String getDivndesc() {
		return divndesc;
	}

	public void setDivndesc(String divndesc) {
		this.divndesc = divndesc;
	}

	public String getDivnaddress1() {
		return divnaddress1;
	}

	public void setDivnaddress1(String divnaddress1) {
		this.divnaddress1 = divnaddress1;
	}
	
	public String getDivnaddress2() {
		return divnaddress2;
	}

	public void setDivnaddress2(String divnaddress2) {
		this.divnaddress2 = divnaddress2;
	}
	
	public String getDivncity() {
		return divncity;
	}

	public void setDivncity(String divncity) {
		this.divncity = divncity;
	}
	
	public String getDivnstate() {
		return divnstate;
	}

	public void setDivnstate(String divnstate) {
		this.divnstate = divnstate;
	}
	
	public String getDivnzip() {
		return divnzip;
	}

	public void setDivnzip(String divnzip) {
		this.divnzip = divnzip;
	}
	
	public String getDivncountry() {
		return divncountry;
	}

	public void setDivncountry(String divncountry) {
		this.divncountry = divncountry;
	}
	
	public String getDivncontactname() {
		return divncontactname;
	}

	public void setDivncontactname(String divncontactname) {
		this.divncontactname = divncontactname;
	}
	
	public String getDivncontactnumber() {
		return divncontactnumber;
	}

	public void setDivncontactnumber(String divncontactnumber) {
		this.divncontactnumber = divncontactnumber;
	}
	
	/*public String getDivncreatetimestamp() {
		return divncreatetimestamp;
	}

	public void setDivncreatetimestamp(String divncreatetimestamp) {
		this.divncreatetimestamp = divncreatetimestamp;
	}
	
	public String getDivnchangetimestamp() {
		return divnchangetimestamp;
	}

	public void setDivnchangetimestamp(String divnchangetimestamp) {
		this.divnchangetimestamp = divnchangetimestamp;
	}
	
	public String getDivncreateuser() {
		return divncreateuser;
	}

	public void setDivncreateuser(String divncreateuser) {
		this.divncreateuser = divncreateuser;
	}
	
	public String getDivnchangeuser() {
		return divnchangeuser;
	}

	public void setDivnchangeuser(String divnchangeuser) {
		this.divnchangeuser = divnchangeuser;
	}*/
	
	/*public Company getCompany() {
		return company;
	}
	
	public void setCompany(Company company) {
		this.company=company;
	}
	*/
	public static long getSerialversionuid() {
		return serialVersionUID;
}
}