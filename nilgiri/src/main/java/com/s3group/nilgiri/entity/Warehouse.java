package com.s3group.nilgiri.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="WAREHOUSE")

public class Warehouse implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="WAREHOUSE_ID")
	private Long warehouseId;
	
	@Column(name="COMPANY_ID")
	private Long companyId;
	
	@Column(name="DIVISION_ID")
	private Long divisionId;
	
	
	@Column(name= "WHSE_WAREHOUSE")
	private String whsewarehouse;
	
	@Column(name="WHSE_DESC")
	 private String whsedesc;
	   
	 @Column(name="WHSE_ADDRESS1")
	 private String whseaddress1;
	   
	 @Column(name="WHSE_ADDRESS2")
	 private String whseaddress2;
	   
	 @Column(name="WHSE_CITY")
	 private String whsecity;
	   
	 @Column(name="WHSE_STATE")
	 private String whsestate;
	   
	 @Column(name="WHSE_ZIP")
	 private String whsezip;
	   
	 @Column(name="WHSE_COUNTRY")
	 private String whsecountry;
	   
	 @Column(name="WHSE_CONTACT_NAME")
	 private String whsecontactname;
	   
	 @Column(name="WHSE_CONTACT_NUMBER")
	 private String whsecontactnumber;
	 
	 @Column(name="WHSE_ALT_ADDRESS1")
	 private String whsealtaddress1;
	   
	 @Column(name="WHSE_ALT_ADDRESS2")
	 private String whsealtaddress2;
	   
	 @Column(name="WHSE_ALT_CITY")
	 private String whsealtcity;
	   
	 @Column(name="WHSE_ALT_STATE")
	 private String whsealtstate;
	   
	 @Column(name="WHSE_ALT_ZIP")
	 private String whsealtzip;
	 
	 @Column(name="WHSE_ALT_COUNTRY")
	 private String whsealtcountry;
	 
	/* @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "COMPANY_ID", nullable=false)
	 	private Company company;
	 
	 
	 @ManyToOne(fetch = FetchType.LAZY)
	 @JoinColumn(name = "DIVISION_ID", nullable=false)
	 	private Division division;
	 
		public Warehouse() {
			
		}
	 */
	 
	    public Long getCompanyId()
	    {
	        return companyId;
	    }

	    public void setCompanyId(Long companyId) 
	    {
	        this.companyId = companyId;
	    }
	 
	    public Long getDivisionId()
	    {
	        return divisionId;
	    }

	    public void setDivisionId(Long divisionId) 
	   {
	        this.divisionId = divisionId;
	   }
	    
	    public Long getWarehouseId()
	    {
	        return warehouseId;
	    }

	    public void setWarehouseId(Long warehouseId) 
	   {
	        this.warehouseId = warehouseId;
	   }
	   
	    public String getWhseWarehouse() {
	        return whsewarehouse;
	    }

	    public void setWhseWarehouse(String WHSE_Warehouse) {
	        this.whsewarehouse = WHSE_Warehouse;
	    }
	   
	    public String getWhseDesc() {
	        return whsedesc;
	    }

	    public void setWhseDesc(String whsedesc) {
	        this.whsedesc = whsedesc;
	    }

	    public String getWhseAddress1() {
	        return whseaddress1;
	    }

	    public void setWhseAddress1(String whseaddress1) {
	        this.whseaddress1 = whseaddress1;
	    }
	   
	    public String getWhseAddress2() {
	        return whseaddress2;
	    }

	    public void setWhseAddress2(String whseaddress2) {
	        this.whseaddress2 = whseaddress2;
	    }
	   
	    public String getWhseCity() {
	        return whsecity;
	    }

	    public void setWhseCity(String whsecity) {
	        this.whsecity = whsecity;
	    }
	   
	    public String getWhseState() {
	        return whsestate;
	    }

	    public void setWhseState(String whsestate) {
	        this.whsestate = whsestate;
	    }
	   
	    public String getWhseZip() {
	        return whsezip;
	    }

	    public void setWhseZip(String whsezip) {
	        this.whsezip = whsezip;
	    }
	   
	    public String getWhseCountry() {
	        return whsecountry;
	    }

	    public void setWhseCountry(String whsecountry) {
	        this.whsecountry = whsecountry;
	    }
	   
	    public String getWhseContactname() {
	        return whsecontactname;
	    }

	    public void setWhseContactname(String whsecontactname) {
	        this.whsecontactname = whsecontactname;
	    }
	   
	    public String getWhseContactnumber() {
	        return whsecontactnumber;
	    }

	    public void setWhseContactnumber(String whsecontactnumber) {
	        this.whsecontactnumber = whsecontactnumber;
	    }
	    public String getWhseAltAddress1() {
	        return whsealtaddress1;
	    }

	    public void setWhseAltAddress1(String whsealtaddress1) {
	        this.whsealtaddress1 = whsealtaddress1;
	    }
	    
	    public String getWhseAltAddress2() {
	        return whsealtaddress2;
	    }

	    public void setWhseAltAddress2(String whsealtaddress2) {
	        this.whsealtaddress2 = whsealtaddress2;
	        
	    }
	    
	    public String getWhseAltCity() {
	        return whsealtcity;
	    }

	    public void setWhseAltCity(String whsealtcity) {
	        this.whsealtcity = whsealtcity;
	    }
	    
	    public String getWhseAltState() {
	        return whsealtcity;
	    }

	    public void setWhseAltState(String whsealtstate) {
	        this.whsealtstate = whsealtstate;
	    }
	    public String getWhseAltZip() {
	        return whsealtzip;
	    }

	    public void setWhseAltZip(String whsealtzip) {
	        this.whsealtzip = whsealtzip;
	    }
	    public String getWhseAltCountry() {
	        return whsealtcountry;
	    }

	    public void setWhseAltCountry(String whsealtcountry) {
	        this.whsealtcountry = whsealtcountry;
	    }
	   
	    public static long getSerialversionuid() {
	        return serialVersionUID;
	}
	

}