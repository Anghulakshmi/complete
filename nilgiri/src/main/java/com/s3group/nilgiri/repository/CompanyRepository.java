package com.s3group.nilgiri.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.s3group.nilgiri.entity.Company;


@Repository

public interface CompanyRepository extends PagingAndSortingRepository<Company,Long> {
	
	Company findByCompanyId(Long id);

}
