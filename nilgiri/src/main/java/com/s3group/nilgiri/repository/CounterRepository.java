package com.s3group.nilgiri.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.s3group.nilgiri.entity.Counter;


@Repository
public interface CounterRepository  extends PagingAndSortingRepository<Counter,Long> {
	
	Counter findByCounterId(Long id);

}
