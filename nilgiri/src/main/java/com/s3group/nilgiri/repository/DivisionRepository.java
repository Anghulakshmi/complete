package com.s3group.nilgiri.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.s3group.nilgiri.entity.Division;


@Repository

public interface DivisionRepository extends PagingAndSortingRepository<Division,Long> {
	
	Division findByDivisionId(Long id);

}
