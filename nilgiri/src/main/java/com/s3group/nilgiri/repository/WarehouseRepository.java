package com.s3group.nilgiri.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.s3group.nilgiri.entity.Warehouse;

@Repository
public interface WarehouseRepository extends PagingAndSortingRepository<Warehouse,Long> {
		
		Warehouse findByWarehouseId(Long id);

	}
