package com.s3group.nilgiri.service;

import java.util.List;

import com.s3group.nilgiri.dto.CompanyDTO;
import com.s3group.nilgiri.entity.Company;


public interface CompanyService {

public List<CompanyDTO> findAllCompanys();
	
	public List<CompanyDTO> saveCompany(CompanyDTO companyDTO);
	
	public List<CompanyDTO> updateCompany(CompanyDTO companyDTO);
	
	public void deleteCompany(Long id);
	
	public Company findByCompanyId(Long id);
}
