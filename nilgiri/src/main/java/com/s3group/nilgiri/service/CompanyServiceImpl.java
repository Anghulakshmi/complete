package com.s3group.nilgiri.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.s3group.nilgiri.dao.CompanyDAO;
import com.s3group.nilgiri.dto.CompanyDTO;
import com.s3group.nilgiri.entity.Company;


@Service
public class CompanyServiceImpl implements CompanyService {
	
	public static final Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);
	
	/*@Autowired
	private CompanyRepository companyRepository;*/
	
	@Autowired
	private CompanyDAO companyDAO;
	
	public List<CompanyDTO> findAllCompanys(){				
		List <Company> companyLists = companyDAO.findAllCompanys();
		//List <Company> companyLists = companyRepository.findAll();		
		List<CompanyDTO> companyDTOs = new ArrayList<CompanyDTO>();
		for(Company company : companyLists) {
			CompanyDTO companyDTO = buildCompanyDTO(company);	
			companyDTOs.add(companyDTO);
		}	
		return companyDTOs;
	}	
	
	public List<CompanyDTO> saveCompany(CompanyDTO companyDTO) {			
		Company company = buildCompany(companyDTO);

		List <Company> companyLists = companyDAO.saveCompany(company);
		
		List<CompanyDTO> companyDTOs = new ArrayList<CompanyDTO>();
		for(Company ct : companyLists) {
			CompanyDTO ctDTO = buildCompanyDTO(ct);	
			companyDTOs.add(ctDTO);
		}
		//companyRepository.save(company);
		return companyDTOs;
	}
	
	public List<CompanyDTO> updateCompany(CompanyDTO companyDTO) {
		Company company = buildCompany(companyDTO);	
		List <Company> companyLists = companyDAO.updateCompany(company);
		List<CompanyDTO> companyDTOs = new ArrayList<CompanyDTO>();
		for(Company ct : companyLists) {
			CompanyDTO ctDTO = buildCompanyDTO(ct);	
			companyDTOs.add(ctDTO);
		}
		//companyRepository.save(company);
		return companyDTOs;
	}

	public void deleteCompany(Long id) {
		companyDAO.deleteCompany(id);
		//companyRepository.deleteById(id);			
	}
	
	public Company findByCompanyId(Long id) {
		Company company = companyDAO.findByCompanyId(id);
		//Company company = companyRepository.findByCompanyId(id);	
		return company;
	}
	
	private Company buildCompany(CompanyDTO companyDTO) {		
		Company company = new Company();		
		company.setCompanyId(companyDTO.getId());
		company.setCompcompany(companyDTO.getcompany());
		company.setCompdesc(companyDTO.getdesc());
		company.setCompaddress1(companyDTO.getaddress1());
		company.setCompaddress2(companyDTO.getaddress2());
		company.setCompcity(companyDTO.getcity());
		company.setCompstate(companyDTO.getstate());
		company.setCompzip(companyDTO.getzip());
		company.setCompcountry(companyDTO.getcountry());
		company.setCompcontactname(companyDTO.getcontactname());
		company.setCompcontactnumber(companyDTO.getcontactnumber());
        //logger.info("Buildcompany : ", company.getCompcompany());
		return company;
	}
	
	private CompanyDTO buildCompanyDTO(Company company) {					
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setId(company.getCompanyId());
		companyDTO.setcompany(company.getCompcompany());
		companyDTO.setdesc(company.getCompdesc());
		companyDTO.setaddress1(company.getCompaddress1());
		companyDTO.setaddress2(company.getCompanyaddress2());
		companyDTO.setcity(company.getCompcity());
		companyDTO.setstate(company.getCompstate());
		companyDTO.setzip(company.getCompzip());
		companyDTO.setcountry(company.getCompcountry());
		companyDTO.setcontactname(company.getCompcontactname());
		companyDTO.setcontactnumber(company.getCompcontactnumber());
		
		return companyDTO;
	}	
	

}
