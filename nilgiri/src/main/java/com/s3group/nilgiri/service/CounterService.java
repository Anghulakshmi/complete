package com.s3group.nilgiri.service;

import java.util.List;

import com.s3group.nilgiri.dto.CounterDTO;
import com.s3group.nilgiri.entity.Counter;

public interface CounterService {
	
	public List<CounterDTO> findAllCounters();
	
	public List<CounterDTO> saveCounter(CounterDTO counterDTO);
	
	public List<CounterDTO> updateCounter(CounterDTO counterDTO);
	
	public void deleteCounter(Long id);
	
	public Counter findByCounterId(Long id);

}
