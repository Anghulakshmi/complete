package com.s3group.nilgiri.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.s3group.nilgiri.dao.CounterDAO;
import com.s3group.nilgiri.dto.CounterDTO;
import com.s3group.nilgiri.entity.Counter;

@Service
public class CounterServiceImpl implements CounterService{
	
	public static final Logger logger = LoggerFactory.getLogger(CounterServiceImpl.class);
					
	/*@Autowired
	private CounterRepository counterRepository;*/
	
	@Autowired
	private CounterDAO counterDAO;
	
	public List<CounterDTO> findAllCounters(){				
		List <Counter> counterLists = counterDAO.findAllCounters();
		//List <Counter> counterLists = counterRepository.findAll();		
		List<CounterDTO> counterDTOs = new ArrayList<CounterDTO>();
		for(Counter counter : counterLists) {
			CounterDTO counterDTO = buildCounterDTO(counter);	
			counterDTOs.add(counterDTO);
		}	
		return counterDTOs;
	}	
	
	public List<CounterDTO> saveCounter(CounterDTO counterDTO) {			
		Counter counter = buildCounter(counterDTO);		
		List <Counter> counterLists = counterDAO.saveCounter(counter);	
		List<CounterDTO> counterDTOs = new ArrayList<CounterDTO>();
		for(Counter ct : counterLists) {
			CounterDTO ctDTO = buildCounterDTO(ct);	
			counterDTOs.add(ctDTO);
		}
		//counterRepository.save(counter);
		return counterDTOs;
	}
	
	public List<CounterDTO> updateCounter(CounterDTO counterDTO) {
		Counter counter = buildCounter(counterDTO);	
		List <Counter> counterLists = counterDAO.updateCounter(counter);
		List<CounterDTO> counterDTOs = new ArrayList<CounterDTO>();
		for(Counter ct : counterLists) {
			CounterDTO ctDTO = buildCounterDTO(ct);	
			counterDTOs.add(ctDTO);
		}
		//counterRepository.save(counter);
		return counterDTOs;
	}

	public void deleteCounter(Long id) {
		counterDAO.deleteCounter(id);
		//counterRepository.deleteById(id);			
	}
	
	public Counter findByCounterId(Long id) {
		Counter counter = counterDAO.findByCounterId(id);
		//Counter counter = counterRepository.findByCounterId(id);	
		return counter;
	}
	
	private Counter buildCounter(CounterDTO counterDTO) {		
		Counter counter = new Counter();		
		counter.setCounterId(counterDTO.getId());
		counter.setCompany(counterDTO.getCompany());
		counter.setDivision(counterDTO.getDivision());
		counter.setWarehouse(counterDTO.getWarehouse());
		counter.setRecordType(counterDTO.getRecordType());
		counter.setDescription(counterDTO.getDescription());
		counter.setPrefix(counterDTO.getPrefix());
		counter.setPreLength(counterDTO.getPreLength());
		counter.setEndNumber(counterDTO.getEndNumber());
		counter.setStartNumber(counterDTO.getStartNumber());
		counter.setCurrentNumber(counterDTO.getCurrentNumber());
		counter.setCurrentNumberLength(counterDTO.getCurrentNumberLength());
		counter.setIncrementNumber(counterDTO.getIncrementNumber());
		counter.setCheckDigitType(counterDTO.getCheckDigitType());
		counter.setCheckDigitLength(counterDTO.getCheckDigitLength());
		counter.setResetFlag(counterDTO.getResetFlag());
		counter.setCheckDigitFlag(counterDTO.getCheckDigitFlag());
		counter.setSystemExpansion(counterDTO.getSystemExpansion());
		counter.setCustomerExpansion(counterDTO.getCustomerExpansion());
		return counter;
	}
	
	private CounterDTO buildCounterDTO(Counter counter) {					
		CounterDTO counterDTO = new CounterDTO();
		counterDTO.setId(counter.getCounterId());
		counterDTO.setCompany(counter.getCompany());
		counterDTO.setDivision(counter.getDivision());
		counterDTO.setWarehouse(counter.getWarehouse());
		counterDTO.setRecordType(counter.getRecordType());
		counterDTO.setDescription(counter.getDescription());
		counterDTO.setPrefix(counter.getPrefix());
		counterDTO.setPreLength(counter.getPreLength());
		counterDTO.setEndNumber(counter.getEndNumber());
		counterDTO.setStartNumber(counter.getStartNumber());
		counterDTO.setCurrentNumber(counter.getCurrentNumber());
		counterDTO.setCurrentNumberLength(counter.getCurrentNumberLength());
		counterDTO.setIncrementNumber(counter.getIncrementNumber());
		counterDTO.setCheckDigitType(counter.getCheckDigitType());
		counterDTO.setCheckDigitLength(counter.getCheckDigitLength());
		counterDTO.setResetFlag(counter.getResetFlag());
		counterDTO.setCheckDigitFlag(counter.getCheckDigitFlag());
		counterDTO.setSystemExpansion(counter.getSystemExpansion());
		counterDTO.setCustomerExpansion(counter.getCustomerExpansion());
		return counterDTO;
	}	
	
}