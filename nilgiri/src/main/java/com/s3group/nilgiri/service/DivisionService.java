package com.s3group.nilgiri.service;

import java.util.List;

import com.s3group.nilgiri.dto.DivisionDTO;
import com.s3group.nilgiri.entity.Division;


public interface DivisionService {

public List<DivisionDTO> findAllDivisions();
	
	public List<DivisionDTO> saveDivision(DivisionDTO divisionDTO);
	
	public List<DivisionDTO> updateDivision(DivisionDTO divisionDTO);
	
	public void deleteDivision(Long id);
	
	public Division findByDivisionId(Long id);
}
