package com.s3group.nilgiri.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.s3group.nilgiri.dao.DivisionDAO;
import com.s3group.nilgiri.dto.DivisionDTO;
import com.s3group.nilgiri.entity.Division;

@Service

public class DivisionServiceImpl implements DivisionService {
	
	public static final Logger logger = LoggerFactory.getLogger(DivisionServiceImpl.class);
	
	/*@Autowired
	private DivisionRepository divisionRepository;*/
	
	@Autowired
	private DivisionDAO divisionDAO;
	
	public List<DivisionDTO> findAllDivisions(){				
		List <Division> divisionLists = divisionDAO.findAllDivisions();
		//List <Division> divisionLists = divisionRepository.findAll();		
		List<DivisionDTO> divisionDTOs = new ArrayList<DivisionDTO>();
		for(Division division : divisionLists) {
			DivisionDTO divisionDTO = buildDivisionDTO(division);	
			divisionDTOs.add(divisionDTO);
		}	
		return divisionDTOs;
	}	
	
	public List<DivisionDTO> saveDivision(DivisionDTO divisionDTO) {			
		Division division = buildDivision(divisionDTO);		
		List <Division> divisionLists = divisionDAO.saveDivision(division);	
		List<DivisionDTO> divisionDTOs = new ArrayList<DivisionDTO>();
		for(Division ct : divisionLists) {
			DivisionDTO ctDTO = buildDivisionDTO(ct);	
			divisionDTOs.add(ctDTO);
		}
		//divisionRepository.save(division);
		return divisionDTOs;
	}
	
	public List<DivisionDTO> updateDivision(DivisionDTO divisionDTO) {
		Division division = buildDivision(divisionDTO);	
		List <Division> divisionLists = divisionDAO.updateDivision(division);
		List<DivisionDTO> divisionDTOs = new ArrayList<DivisionDTO>();
		for(Division ct : divisionLists) {
			DivisionDTO ctDTO = buildDivisionDTO(ct);	
			divisionDTOs.add(ctDTO);
		}
		//divisionRepository.save(division);
		return divisionDTOs;
	}

	public void deleteDivision(Long id) {
		divisionDAO.deleteDivision(id);
		//divisionRepository.deleteById(id);			
	}
	
	public Division findByDivisionId(Long id) {
		Division division = divisionDAO.findByDivisionId(id);
		//Division division = divisionRepository.findByDivisionId(id);	
		return division;
	}
	
	private Division buildDivision(DivisionDTO divisionDTO) {		
		Division division = new Division();	
		
		//division.setCompany(divisionDTO.getcompany());
		
		division.setCompanyId(divisionDTO.getCompanyId());
		division.setDivisionId(divisionDTO.getId());
		division.setDivndivision(divisionDTO.getdivision());
		division.setDivndesc(divisionDTO.getdesc());
		division.setDivnaddress1(divisionDTO.getaddress1());
		division.setDivnaddress2(divisionDTO.getaddress2());
		division.setDivncity(divisionDTO.getcity());
		division.setDivnstate(divisionDTO.getstate());
		division.setDivnzip(divisionDTO.getzip());
		division.setDivncountry(divisionDTO.getcountry());
		division.setDivncontactname(divisionDTO.getcontactname());
		division.setDivncontactnumber(divisionDTO.getcontactnumber());
		return division;
	}
	
	private DivisionDTO buildDivisionDTO(Division division) {					
		DivisionDTO divisionDTO = new DivisionDTO();
		
		//divisionDTO.setcompany(division.getCompany());
		divisionDTO.setCompanyId(division.getCompanyId());
		divisionDTO.setId(division.getDivisionId());
		divisionDTO.setdivision(division.getDivndivision());
		divisionDTO.setdesc(division.getDivndesc());
		divisionDTO.setaddress1(division.getDivnaddress1());
		divisionDTO.setaddress2(division.getDivnaddress2());
		divisionDTO.setcity(division.getDivncity());
		divisionDTO.setstate(division.getDivnstate());
		divisionDTO.setzip(division.getDivnzip());
		divisionDTO.setcountry(division.getDivncountry());
		divisionDTO.setcontactname(division.getDivncontactname());
		divisionDTO.setcontactnumber(division.getDivncontactnumber());
		
		return divisionDTO;
	}	
	

}
