package com.s3group.nilgiri.service;

import java.util.List;

import com.s3group.nilgiri.dto.LocationDTO;
import com.s3group.nilgiri.entity.Location;

public interface LocationService {

	public List<LocationDTO> findAllLocations();
	
	public List<LocationDTO> saveLocation(LocationDTO locationDTO);
	
	public List<LocationDTO> updateLocation(LocationDTO locationDTO);
	
	public void deleteLocation(Long id);
	
	public Location findByLocationId(Long id);
}
