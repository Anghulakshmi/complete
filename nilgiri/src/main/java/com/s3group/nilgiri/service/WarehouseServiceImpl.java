package com.s3group.nilgiri.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.s3group.nilgiri.dao.WarehouseDAO;
import com.s3group.nilgiri.dto.WarehouseDTO;
import com.s3group.nilgiri.entity.Warehouse;


@Service
public class WarehouseServiceImpl implements WarehouseService{
	
		public static final Logger logger = LoggerFactory.getLogger(WarehouseServiceImpl.class);
						
		/*@Autowired
		private WarehouseRepository warehouseRepository;*/
		
		@Autowired
		private WarehouseDAO warehouseDAO;
		
		public List<WarehouseDTO> findAllWarehouses(){				
			List <Warehouse> warehouseLists = warehouseDAO.findAllWarehouses();
			//List <Warehouse> warehouseLists = warehouseRepository.findAll();		
			List<WarehouseDTO> warehouseDTOs = new ArrayList<WarehouseDTO>();
			for(Warehouse warehouse: warehouseLists) {
				WarehouseDTO warehouseDTO = buildWarehouseDTO(warehouse);	
				warehouseDTOs.add(warehouseDTO);
			}	
			return warehouseDTOs;
		}	
		
		public List<WarehouseDTO> saveWarehouse(WarehouseDTO warehouseDTO) {			
			Warehouse warehouse= buildWarehouse(warehouseDTO);		
			List <Warehouse> warehouseLists = warehouseDAO.saveWarehouse(warehouse);	
			List<WarehouseDTO> warehouseDTOs = new ArrayList<WarehouseDTO>();
			for(Warehouse wh: warehouseLists) {
				WarehouseDTO whDTO = buildWarehouseDTO(wh);	
				warehouseDTOs.add(whDTO);
			}
			//warehouseRepository.save(warehouse);
			return warehouseDTOs;
		}
		
		public List<WarehouseDTO> updateWarehouse(WarehouseDTO warehouseDTO) {
			Warehouse warehouse = buildWarehouse(warehouseDTO);	
			List <Warehouse> warehouseLists = warehouseDAO.updateWarehouse(warehouse);
			List<WarehouseDTO> warehouseDTOs = new ArrayList<WarehouseDTO>();
			for(Warehouse wh : warehouseLists) {
				WarehouseDTO whDTO = buildWarehouseDTO(wh);	
				warehouseDTOs.add(whDTO);
			}
			//warehouseRepository.save(warehouse);
			return warehouseDTOs;
		}

		public void deleteWarehouse(Long warehouseId) {
			warehouseDAO.deleteWarehouse(warehouseId);
			//warehouseRepository.deleteById(warehouseId);			
		}
		
		public Warehouse findByWarehouseId(Long warehouseId) {
			Warehouse warehouse = warehouseDAO.findByWarehouseId(warehouseId);
			//Warehouse warehouse = warehouseRepository.findByWarehouseId(id);	
			return warehouse;
		}
		
		private Warehouse buildWarehouse(WarehouseDTO warehouseDTO) {		
			Warehouse warehouse = new Warehouse();		
			
			warehouse.setCompanyId(warehouseDTO.getCompanyId());
			warehouse.setDivisionId(warehouseDTO.getDivisionId());
			warehouse.setWarehouseId(warehouseDTO.getWarehouseId());
			warehouse.setWhseWarehouse(warehouseDTO.getWarehouse());
			warehouse.setWhseDesc(warehouseDTO.getDesc());
			warehouse.setWhseAddress1(warehouseDTO.getAddress1());
			warehouse.setWhseAddress2(warehouseDTO.getAddress2());
			warehouse.setWhseCity(warehouseDTO.getCity());
			warehouse.setWhseState(warehouseDTO.getState());
			warehouse.setWhseZip(warehouseDTO.getZip());
			warehouse.setWhseCountry(warehouseDTO.getCountry());
			warehouse.setWhseContactname(warehouseDTO.getContactName());
			warehouse.setWhseContactnumber(warehouseDTO.getContactNumber());
			warehouse.setWhseAltAddress1(warehouseDTO.getAltAddress1());
			warehouse.setWhseAltAddress2(warehouseDTO.getAltAddress2());
			warehouse.setWhseAltCity(warehouseDTO.getAltCity());
			warehouse.setWhseAltState(warehouseDTO.getAltState());
			warehouse.setWhseAltZip(warehouseDTO.getAltZip());
			warehouse.setWhseAltCountry(warehouseDTO.getAltCountry());

			return warehouse;
		}
		
		private WarehouseDTO buildWarehouseDTO(Warehouse warehouse) {					
			WarehouseDTO warehouseDTO = new WarehouseDTO();
			
			warehouseDTO.setCompanyId(warehouse.getCompanyId());
			warehouseDTO.setDivisionId(warehouse.getDivisionId());
			warehouseDTO.setWarehouseId(warehouse.getWarehouseId());
			warehouseDTO.setWarehouse(warehouse.getWhseWarehouse());
			warehouseDTO.setDesc(warehouse.getWhseDesc());
			warehouseDTO.setAddress1(warehouse.getWhseAddress1());
			warehouseDTO.setAddress2(warehouse.getWhseAddress1());
			warehouseDTO.setCity(warehouse.getWhseCity());
			warehouseDTO.setState(warehouse.getWhseState());
			warehouseDTO.setZip(warehouse.getWhseZip());
			warehouseDTO.setCountry(warehouse.getWhseCountry());
			warehouseDTO.setContactName(warehouse.getWhseContactname());
			warehouseDTO.setContactNumber(warehouse.getWhseContactnumber());
			warehouseDTO.setAltAddress1(warehouse.getWhseAltAddress1 ());
			warehouseDTO.setAltAddress2(warehouse.getWhseAltAddress2());
			warehouseDTO.setAltCity(warehouse.getWhseAltCity());
			warehouseDTO.setAltState(warehouse.getWhseAltState());
			warehouseDTO.setAltZip(warehouse.getWhseAltZip());
			warehouseDTO.setAltCountry(warehouse.getWhseAltCountry());
			return warehouseDTO;
		}	
		
	}
