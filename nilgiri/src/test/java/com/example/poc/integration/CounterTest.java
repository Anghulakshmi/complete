package com.example.poc.integration;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.s3group.nilgiri.dao.CounterDAO;
import com.s3group.nilgiri.dto.CounterDTO;
import com.s3group.nilgiri.service.CounterService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {})
public class CounterTest {
	
	public static final Logger logger = LoggerFactory.getLogger(CounterTest.class);
	
	@Autowired
	private CounterService counterService;
	
	@Autowired
	private CounterDAO counterDAO;
		
	@PersistenceContext
	EntityManager entityManager;
		
	@Before
	public void init() throws Exception{	
				
	}
	
	@Test
	public void testAndValidate() {
		CounterDTO counterDTO = new CounterDTO();
		
		counterDTO.setCompany("ABC");
		counterDTO.setDivision("Sounth");
		counterDTO.setWarehouse("Bengaluru");
		counterDTO.setRecordType("R");
		counterDTO.setDescription("Counter for milk products");
		counterDTO.setPrefix("P12345");
		counterDTO.setPreLength(5.0);
		counterDTO.setEndNumber(20.23);
		counterDTO.setStartNumber(10.5);
		counterDTO.setCurrentNumber(15.25);
		counterDTO.setCurrentNumberLength(2.0);
		counterDTO.setIncrementNumber(1.0);
		counterDTO.setCheckDigitType("AB12");
		counterDTO.setCheckDigitLength(3.0);
		counterDTO.setResetFlag('T');
		counterDTO.setCheckDigitFlag('F');
		counterDTO.setSystemExpansion("Exapnsion");
		counterDTO.setCustomerExpansion("Customer");
		
		List<CounterDTO> counterDTOs = counterService.saveCounter(counterDTO);
		
		for(CounterDTO ct : counterDTOs) {
			logger.info("Output results {} "+ ct.getCompany() + "{}" + ct.getDivision() + "{}" + ct.getWarehouse() + "{}" + ct.getRecordType() + "{}" + ct.getDescription());
		}
		
	}
	

}
